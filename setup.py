from setuptools import find_packages, setup
from opconnect import __version__

setup(
    name="let-op-portal-connect",
    version=__version__,
    author="Bengt Giger",
    author_email="bgiger@ethz.ch",
    description="Read seat assignments for Linux exams",
    packages=find_packages(),
    py_modules=["opconnect"],
    entry_points={
        "console_scripts": [
            "opconnect = opconnect.main:fetch",
        ]
    },
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    url="https://gitlab.ethz.ch/id-cd-lnx/computerrooms/let-op-portal-connect",
    install_requires=["click", "requests"],
)
