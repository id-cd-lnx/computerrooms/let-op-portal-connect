# let-op-portal-connect

Read seat assignments from LET OP portal.

## Installation
```shell
pip3 install let-op-portal-connect --extra-index-url https://gitlab.ethz.ch/api/v4/projects/27163/packages/pypi/simple@<git tag>
```

To access this registry without `--extra-index-url`, add this to `.pypirc`:

```
[gitlab]
repository = https://gitlab.ethz.ch/api/v4/projects/27163/packages/pypi
```

See the *Package Registry* tab for more information.

## Usage
```
Usage: opconnect [OPTIONS] IP LK_NUMBER

  opconnect: Retrieve workstation seat configuration from LET OP portal.

  Mandatory arguments:

  IP:         ip address for current seat

  LK_NUMBER:  exam identifier

Options:
  -o, --output TEXT      Path to output file
  -t, --test             Don't use network connection, but internal dummy data
                         (192.168.0.3, .4)
  -q, --quiet            Suppress normal output
  --help                 Show this message and exit.
```
Access to the OP portal requires an access key. Define the key as environment variable:


# Testing
For a simple test, use the option `-t` with IPs 192.168.0.3 or 192.132.0.4, and `LK_NUMBER` 151-0227-00S. The
same arguments may be used to connect to the mockup server. To overwrite the default URI for the OP portal API,
set the environment variable `LET_OPPORTAL_URI`:

```shell
export LET_OPPORTAL_URI="http://idinstallprd.ethz.ch/examreleasetest/opportal.json"
```

Then:

```shell
opconnect 192.168.0.3 151-0227-00S
```
