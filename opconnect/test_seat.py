import json
import unittest
import ipaddress
import opconnect.seat

seat_data = """
{
"ip": "192.168.0.3",
"lastname": "Doe",
"firstname": "John äöü",
"student_id": "johndoe",
"registration": "11-111-11"
}"""


class TestSeat(unittest.TestCase):
    seat_js = None

    def setUp(self) -> None:
        self.seat_js = json.loads(seat_data)

    def test_seat_create(self):
        seat = opconnect.seat.Seat(self.seat_js)
        self.assertEqual(ipaddress.ip_address("192.168.0.3"), seat.ip)
        self.assertEqual("Doe", seat.last_name)
        self.assertEqual("John äöü", seat.first_name)
        self.assertEqual("johndoe", seat.login_name)
        self.assertEqual("11-111-11", seat.legi_id)

    def test_seat_output(self):
        seat = opconnect.seat.Seat(self.seat_js)
        self.assertIn('export LOGINNAME="johndoe"', seat.write())
        self.assertIn('export REALNAME="Doe John äöü"', seat.write())
        self.assertIn('export LEGIID="11-111-11"', seat.write())
