import json
import requests
import logging
import os

# import datetime

opportal_url = "https://op-portal.let.ethz.ch/linux/?key={0}&exam_id={1}"

# for testing
dummy_data = """{
"exam_nr": "151-0227-00S",
"exam_name": "Basics of Air Transport (Aviation I)",
"exam_type": "Moodle",
"exam_date": "2020-03-18",
"participants":
[
    {
        "ip": "192.168.0.3",
        "lastname": "Abdi",
        "firstname": "Fatime",
        "student_id": "fabdi",
        "registration": "16-057-176"
    },
    {
        "ip": "192.168.0.4",
        "lastname": "Aellig",
        "firstname": "Ronja Carina",
        "student_id": "raelli",
        "registration": "20-939-294"
    }
]
}
"""


class Connection(object):
    """
    Handle connections to the portal server API
    """

    def read(self, lk_number=None):
        raise NotImplementedError


class DummyConnection(Connection):
    """
    Work with hardcoded data for offline testing
    """

    def __init__(self, dummy_seat_data=dummy_data):
        self.data = dummy_seat_data

    def read(self, lk_number=None):
        """
        Return hardcoded data for offline testing
        :param lk_number: exam number
        :return:
        """
        return json.loads(self.data)


class PortalConnection(Connection):
    """
    Connect to OP portal API
    """

    def __init__(self, uri=None):
        """
        Create connector to URI givven
        :param uri: URI, optionally with placeholder for exam number (https://a.b.c/api?key={0}exam={1}). Env variable LET_OPPORTAL_URI overwrites URI
        """
        self.key = os.getenv("LET_OPPORTAL_KEY")
        if self.key is None:
            logging.error("LET_OPPORTAL_KEY variable not set")
            exit(1)
        if uri is None:
            self.uri = os.getenv("LET_OPPORTAL_URI", opportal_url)
        else:
            self.uri = uri

    def read(self, lk_number=None):
        """
        Read data for an exam
        :param lk_number: exam number
        :return:
        """
        if lk_number is None:
            logging.error("LK number is missing")
            exit(1)
        r = requests.get(self.uri.format(self.key, lk_number))
        if r.status_code < 400:
            return r.json()
        else:
            return None
