from opconnect.__init__ import __version__
import opconnect.connection as connection
from opconnect.seat import Seats
import click
import ipaddress
import logging


@click.command()
@click.argument("ip")
@click.argument("lk_number")
@click.option("--output", "-o", default=".exam-setup-user", help="Path to output file")
@click.option(
    "--test",
    "-t",
    is_flag=True,
    help="Don't use network connection, but internal dummy data (192.168.0.3, .4)",
)
@click.option("--quiet", "-q", is_flag=True, help="Suppress normal output")
@click.version_option()
def fetch(ip, lk_number, output, test=False, quiet=False):
    """
    opconnect: Retrieve workstation seat configuration from LET OP portal.

    Environment variables:
    - LET_OPPORTAL_KEY for the access key
    - LET_OPPORTAL_URI (optional) to overwrite the default URI. The URI must have the format
      https://a.b.c/api?key={0}exam={1}. Optionally append &accepted to retrieve ALL seats, not only the confirmed ones.

    Mandatory arguments:

    IP:         ip address for current seat

    LK_NUMBER:  exam identifier
    """
    if not quiet:
        print("opconnect v{0}".format(__version__))

    my_ip = None
    try:
        my_ip = ipaddress.ip_address(ip)
    except ValueError:  # pragma: no cover
        logging.error("Not a valid IP: {0}".format(ip))
        exit(1)

    # For quick tests, we can use a dummy connector which uses hardcoded test data
    if test:
        connector = connection.DummyConnection()
    else:
        connector = connection.PortalConnection()
    seats = Seats(connector, lk_number=lk_number)

    seat = seats.get(str(my_ip))
    if seat is not None:
        f = None
        try:
            f = open(output, mode="w")
        except Exception as e:  # pragma: no cover
            logging.error(e)
            exit(1)

        f.write(seat.write())
        f.close()
        if not quiet:
            print("Configuration written to {0}".format(output))
        exit(0)
    else:
        logging.error("No matching IP found")
        exit(1)


if __name__ == "__main__":
    fetch()
