import ipaddress
import datetime
import logging

file_template = """# Created by opconnect
export LOGINDATE="{0}"
export LOGINNAME="{1}"
export REALNAME="{2}"
export LEGIID="{3}"
export REMOTEDISPLAY=""
"""


class Seat(object):
    """
    Represents a candidate and the computer (IP) assigned
    """

    ip: None
    last_name = ""
    first_name = ""
    legi_id = ""
    login_name = ""

    def __init__(self, seat_js):
        """
        Initialize from JSON read from OP portal
        :param seat_js: JSON structure containing fields:
        - ip
        - lastname
        - firstname
        - student_id
        """
        ip = seat_js["ip"]
        self.last_name = seat_js["lastname"]
        self.first_name = seat_js["firstname"]
        self.login_name = seat_js["student_id"]
        self.legi_id = seat_js["registration"]
        try:
            self.ip = ipaddress.ip_address(ip)
        except ValueError:
            logging.error(
                "Not a valid IP {0} for participant {1} {2}".format(
                    ip, self.first_name, self.last_name
                )
            )
            logging.error("Aborting execution")
            exit(1)

    def write(self):
        """
        Write the .exam-setup-user file for this seat
        :return:
        """
        dt = datetime.datetime.now()
        return file_template.format(
            dt.ctime(),
            self.login_name,
            self.last_name + " " + self.first_name,
            self.legi_id,
        )


class Seats(list):
    """
    List of all seats defined for an exam
    """

    def __init__(self, connector, lk_number=None):
        list.__init__(self)
        exam_data = connector.read(lk_number)
        for item in exam_data["participants"]:
            self.append(Seat(item))

    def get(self, ip):
        """
        Fetch the seat information for a given computer
        :param ip: IP address identifying the computer
        :return:
        """
        for seat in self:
            try:
                my_ip = ipaddress.ip_address(ip)
            except ValueError:
                logging.error("Not a valid IP: {0}".format(ip))
                continue
            if seat.ip == my_ip:
                return seat
