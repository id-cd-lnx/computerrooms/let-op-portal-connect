import unittest
import opconnect.connection
import opconnect.seat
import tempfile
import json
import logging

data = json.loads(opconnect.connection.dummy_data)
good_ip = data["participants"][0]["ip"]
good_legi = data["participants"][0]["student_id"]


class TestSeats(unittest.TestCase):
    def setUp(self) -> None:
        self.connector = opconnect.connection.DummyConnection(
            opconnect.connection.dummy_data
        )
        self.output = tempfile.TemporaryFile("r+")
        self.seats = opconnect.seat.Seats(self.connector)

    def tearDown(self) -> None:
        self.output.close()

    def test_missing(self):
        self.seats.get("127.0.0.1")
        with self.assertLogs() as lg:
            logging.getLogger().error("No matching IP found")
        self.assertEqual(lg.output, ["ERROR:root:No matching IP found"])

    def test_bad_ip(self):
        bad_ip = "527.0.0.1"
        self.seats.get(bad_ip)
        with self.assertLogs() as lg:
            logging.getLogger().error("Not a valid IP: {0}".format(bad_ip))
        self.assertEqual(lg.output, ["ERROR:root:Not a valid IP: {0}".format(bad_ip)])

    def test_output(self):
        seat = self.seats.get(good_ip)
        self.output.write(seat.write())

        self.output.seek(0)
        result = self.output.read()
        self.assertIn(good_legi, result)
